package com.example.projectfitriNur.model.dto;

import java.io.Serializable;
import java.util.Date;

//import id.siabos.backend.dto.ApiResponse;

public class ApiResponse<T> implements Serializable{
	public static final int OK_CODE = 200;
    public static final int OK_CREATED_CODE = 201;
    public static final int ERROR_CODE_REQUEST = 400;
    public static final int ERROR_CODE_SERVER = 500;
    public static final String SUCCESS = "success";
    public static final String ERROR = "error";

    public ApiResponse() {
    }

    public ApiResponse(int status) {
        this.status = status;
    }

    public ApiResponse(int status, T data, String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    private int status;
    private T data;
    private String message;
    private Date timestamp = new Date();

    public boolean isSuccess() {
        return status == OK_CODE;
    }

    public static <T> ApiResponse<T> ok() {
        return new ApiResponse<>(OK_CODE, null, SUCCESS);
    }

    public static <T> ApiResponse<T> ok(T data) {
        return new ApiResponse<>(OK_CODE, data, SUCCESS);
    }

    public static <T> ApiResponse<T> ok(String message) {
        return new ApiResponse<>(OK_CODE, null, message);
    }

    public static <T> ApiResponse<T> ok(String message, T data) {
        return new ApiResponse<>(OK_CODE, data, message);
    }

    public static <T> ApiResponse<T> error() {
        return new ApiResponse<>(ERROR_CODE_SERVER, null, ERROR);
    }

    public static <T> ApiResponse<T> error(String message) {
        return new ApiResponse<>(ERROR_CODE_SERVER, null, message);
    }

    public static <T> ApiResponse<T> error(T data) {
        return new ApiResponse<>(ERROR_CODE_SERVER, data, ERROR);
    }

    public static <T> ApiResponse<T> error(String message, T data) {
        return new ApiResponse<>(ERROR_CODE_SERVER, data, message);
    }

    public static <T> ApiResponse<T> get(int status, String message, T data) {
        return new ApiResponse<>(status, data, message);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

}
