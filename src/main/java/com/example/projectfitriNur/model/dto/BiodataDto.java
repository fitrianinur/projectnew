package com.example.projectfitriNur.model.dto;

import lombok.Data;

@Data
public class BiodataDto {
	
	private String nama;
	private String alamat;
	private String noHandphone;
	private String nik;
	
}
