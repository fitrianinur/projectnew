package com.example.projectfitriNur.model.dto;

import lombok.Data;

@Data
public class EditBiodata {
	private Long id;
	private String nama;
	private String alamat;
	private String noHandphone;
	private String nik;

}
