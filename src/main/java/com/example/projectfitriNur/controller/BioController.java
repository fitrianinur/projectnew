package com.example.projectfitriNur.controller;

import java.util.List;

import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.projectfitriNur.model.Biodata;
import com.example.projectfitriNur.model.dto.ApiResponse;
import com.example.projectfitriNur.model.dto.BiodataDto;
import com.example.projectfitriNur.model.dto.EditBiodata;
import com.example.projectfitriNur.repository.BiodataRepo;

import io.swagger.annotations.ApiOperation;




@RestController
@RequestMapping("/biodata")
public class BioController {
	private final BiodataRepo biodataRepo;
	
	public BioController(BiodataRepo biodataRepo) {
		this.biodataRepo = biodataRepo;
	}
	
	@PostMapping
	@ApiOperation(value = "API Submit Biodata", response = ApiResponse.class)
	public ResponseEntity<?> insert(HttpServletRequest req, HttpServletResponse resp,
            @RequestBody BiodataDto dto){
		
		Biodata biodata = Biodata.builder()
		         .nama(dto.getNama())
		         .alamat(dto.getAlamat())
		         .noHandphone(dto.getNoHandphone())
		         .nik(dto.getNik())
		         .build();
         biodataRepo.save(biodata);
         return ResponseEntity.ok(ApiResponse.ok(biodata));
	}
//    public BiodataDto insert(@RequestBody BiodataDto dto) {
//        
//		Biodata biodata = Biodata.builder()
//				         .nama(dto.getNama())
//				         .alamat(dto.getAlamat())
//				         .noHandphone(dto.getNoHandphone())
//				         .nik(dto.getNik())
//				         .build();
//		biodataRepo.save(biodata);
//        return dto;
//    }
	
	@GetMapping(value = "/id")
	public Biodata getId(Long id) {
		Biodata biodata = biodataRepo.findById(id).get();
		//BiodataDto biodataDto = new BiodataDto();
		//biodataDto.setId(biodata.getId());
//		biodataDto.setNama(biodata.getNama());
//		biodataDto.setAlamat(biodata.getAlamat());
//		biodataDto.setNoHandphone(biodata.getNoHandphone());
//		biodataDto.setNik(biodata.getNik());
		return biodata;
	}
	
	@DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
		biodataRepo.deleteById(id);
    }

	@GetMapping()
	public List<Biodata> listBiodata(){
		List<Biodata> biodataList = biodataRepo.findAll();
		
		return biodataList;
	}
	
	@PutMapping
	@ApiOperation(value = "API Update Biodata", response = ApiResponse.class)
	public ResponseEntity<?> editBiodata(HttpServletRequest req, HttpServletResponse resp,
            @RequestBody EditBiodata dto){
		Biodata biodata = biodataRepo.findById(dto.getId()).get();
		if(biodata.getId() != null) {
			biodata.setNama(dto.getNama());
			biodata.setAlamat(dto.getAlamat());
			biodata.setNoHandphone(dto.getNoHandphone());
			biodata.setNik(dto.getNik());
			biodataRepo.save(biodata);
		}
		return ResponseEntity.ok(ApiResponse.ok(biodata));
	}
//	public Biodata editBiodata(@RequestBody EditBiodata dto) {
//		Biodata biodata = biodataRepo.findById(dto.getId()).get();
//		biodata.setNama(dto.getNama());
//		biodata.setAlamat(dto.getAlamat());
//		biodata.setNoHandphone(dto.getNoHandphone());
//		biodata.setNik(dto.getNik());
//		biodataRepo.save(biodata);
//		return biodata;
//	}
	
	
	
	
	
	

}
