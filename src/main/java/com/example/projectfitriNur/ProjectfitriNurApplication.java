package com.example.projectfitriNur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectfitriNurApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectfitriNurApplication.class, args);
	}

}
