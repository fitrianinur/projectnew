package com.example.projectFitri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectFitriApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectFitriApplication.class, args);
	}

}
